import React, { Component } from 'react';

import Chat from './chat/Chat';

// Assets
import '../../styles/sass/main.scss';
import 'font-awesome/css/font-awesome.css';

class App extends Component {

    /**
     * Renders the App component.
     */

    render() {
        return (
            <Chat/>
        );
    }

}

export default App;