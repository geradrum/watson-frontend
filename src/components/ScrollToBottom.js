import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';

export default class ScrollToBottom extends Component {

    componentWillUpdate() {
        const node = findDOMNode(this);
        this.shouldScrollBottom = node.scrollTop + node.offsetHeight === node.scrollHeight;
    }

    componentDidUpdate() {
        if (this.shouldScrollBottom) {
            const node = findDOMNode(this);
            const intervalId = setInterval(() => {
                node.scrollTop = node.scrollTop + 10;
                if ((node.scrollTop + node.offsetHeight) >= node.scrollHeight) {
                    clearInterval(intervalId);
                }
            }, 30);
        }
    }

    render() {
        const {className, children} = this.props;
        const style = {
            overflowY: 'scroll',
            ...this.props.style
        };
        return (
            <div {...this.props} style={style}>{ children }</div>
        );
    }
}