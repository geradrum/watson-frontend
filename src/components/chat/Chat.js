import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

import ScrollToBottom from '../ScrollToBottom';
import Message from './Message';
import ChatInput from './ChatInput';

import watsonIcon from '../../../assets/images/watson.png';

const messageReceivedAudio = require('../../../assets/sounds/message_received.mp3');
const messageSentAudio = require('../../../assets/sounds/message_sent.mp3');
const messageReceived = new Audio(messageReceivedAudio);
const messageSent = new Audio(messageSentAudio);

class Chat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            message: ''
        };
    }

    render() {
        return (
            <div id="chat-container">

                <div id="chat-header-container">
                    <img src={watsonIcon} alt=""/>
                </div>

                <ScrollToBottom id="chat-container">
                        
                        <CSSTransitionGroup
                            transitionName="fade-up"
                            transitionEnter={true}
                            transitionLeave={true}
                            transitionEnterTimeout={900}
                            transitionLeaveTimeout={900}
                        >
                            { this.renderMessages() }
                        </CSSTransitionGroup>
                        
                </ScrollToBottom>

                <ChatInput
                    onChange={this.onChange.bind(this)}
                    onSendClick={this.onSendClick.bind(this)}
                    value={this.state.message}
                />

            </div>
        );
    }

    renderMessages() {
        return this.state.messages.map((message, index) => {
            return (
                <Message
                    key={index}
                    text={message.text}
                    from={message.from}
                />
            );
        });
    }

    sendMessage(message) {
        axios.post('http://localhost:4000/api/chat', {
            message
        })
        .then((response) => {
            setTimeout(() => {
                messageReceived.play();
                this.setState({
                    messages: this.state.messages.concat([{
                        text: response.data.output.text[0],
                        from: 'sender'
                    }])
                });
            }, 1000);
        })
        .catch((error) => {
            this.setState({
                messages: [...this.state.messages.slice(0, -1)]
            });
            console.log(error);
        });
    }

    onChange(event) {
        this.setState({
            message: event.target.value
        });
    }

    onSendClick() {
        const message = this.state.message;
        if(message) {
            messageSent.play();
            this.setState({
                messages: this.state.messages.concat([{
                    text: message,
                    from: 'receiver'
                }]),
                message: ''
            });
            this.sendMessage(message);
        }
    }

}

export default Chat;