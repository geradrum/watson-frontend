import React from 'react';
import classNames from 'classnames';

export default (props) => {
    return (
        <div className={classNames('chat-message', props.from)}>
            <span>{props.text}</span>  
        </div>
    );
};