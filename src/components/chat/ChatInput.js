import React, { Component } from 'react';


class ChatInput extends Component {

    render() {
        return (
            <div id="chat-input-container">
                        
                <input id="chat-input" type="text"
                        placeholder="Escribe algo..."
                        onChange={this.props.onChange.bind(this)}
                        onKeyPress={this.onKeyPress.bind(this)}
                        value={this.props.value}   
                />
                <button id="chat-send-button"
                    onClick={this.onSendClick.bind(this)}
                >
                    <i className="fa fa-paper-plane"></i>
                </button>
    
            </div>
        );
    }

    onChange(event) {
        this.props.onChange(event);
    }

    onKeyPress(event) {
        if(event.charCode === 13) {
            this.props.onSendClick();
        }
    }

    onSendClick() {
        this.props.onSendClick();
    }

}

export default ChatInput;